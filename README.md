## Features

[![Build Status](https://drone.services.subpixel.se/api/badges/nuvalis/laravel-criteria-builder/status.svg)](https://drone.services.subpixel.se/nuvalis/laravel-criteria-builder)

- SearchCriteria builder for api usage
- Whitelist columns that are searchable and displayable
- Aliasing mixed database columns eg. [ 'product_id' => 'productId', 'randomShort_LegacyID' => 'properId' ]
- All aliases will be automapped with SearchCriteria builder
- Include only needed columns
- Add associations dynamically with only needed columns
- Laravel Octane tested

## TODO

- Come up with some kind of upsert/batch/patch updater for complete CRUD

# Filter Criteria

### EqualsAnyFilter

EqualsAnyFilter converts to: "where id in (1, 2, 3)"

PHP

````php
$criteria->addFilter(new EqualsAnyFilter('id', [1, 2, 3]));
````

JSON

````json
{
    "filters": [
        {
            "type": "equalsAny",
            "value": [
                1,
                2,
                3
            ],
            "field": "id"
        }
    ]
}
````

### EqualsFilter

EqualsFilter converts to: "where name = 'test'"

PHP

````php
$criteria->addFilter(new EqualsFilter('name', 'test'));
````

JSON

````json
{
    "filters": [
        {
            "type": "equals",
            "value": "test",
            "field": "name"
        }
    ]
}
````

### ContainsFilter

ContainsFilter converts to: "where name like '%test%'"

PHP

````php
$criteria->addFilter(new ContainsFilter('name', 'test'));
````

JSON

````json
{
    "filters": [
        {
            "type": "contains",
            "value": "test",
            "field": "name"
        }
    ]
}
````

### SuffixFilter

SuffixFilter converts to: "where name like '%test'"

PHP

````php
$criteria->addFilter(new SuffixFilter('name', 'test'));
````

JSON

````json
{
    "filters": [
        {
            "type": "contains",
            "value": "test",
            "field": "name"
        }
    ]
}
````

### PrefixFilter

PrefixFilter converts to: "where name like 'test%'"

PHP

````php
$criteria->addFilter(new PrefixFilter('name', 'test'));
````

JSON

````json
{
    "filters": [
        {
            "type": "prefix",
            "value": "test",
            "field": "name"
        }
    ]
}
````

### MultiFilter

MultiFilter takes multiple filters and combines them with either OR or AND Example results in: where name in ('test', '
test2') or name like 'test%'

PHP

````php
$criteria->addFilter(
    new MultiFilter(MultiFilter::CONNECTION_OR, [
        new EqualsAnyFilter('name', ['test', 'test2']),
        new PrefixFilter('name', 'test'),
    ])
);
````

JSON

````json
{
    "filters": [
        {
            "type": "multi",
            "operator": "or",
            "queries": [
                {
                    "type": "equalsAny",
                    "value": [
                        1,
                        2,
                        3
                    ],
                    "field": "id"
                },
                {
                    "type": "prefix",
                    "value": "test",
                    "field": "name"
                }
            ]
        }
    ]
}
````

### NotFilter

NotFilter takes multiple filters and combines them with either OR or AND Example results in: where name not in ('
test', 'test2') and name not like 'test%'

PHP

````php
$criteria->addFilter(
    new NotFilter(NotFilter::CONNECTION_OR, [
        new EqualsAnyFilter('name', ['test', 'test2']),
        new PrefixFilter('name', 'test'),
    ])
);
````

JSON

````json
{
    "filters": [
        {
            "type": "not",
            "operator": "and",
            "queries": [
                {
                    "type": "equalsAny",
                    "value": [
                        1,
                        2,
                        3
                    ],
                    "field": "id"
                },
                {
                    "type": "prefix",
                    "value": "test",
                    "field": "name"
                }
            ]
        }
    ]
}
````

### RangeFilter

RangeFilter WIP, not implemented fully yet

PHP

````php
$criteria->addFilter(
    new RangeFilter('created_at', [
        RangeFilter:GT => '2000-01-01',
        RangeFilter::LT => '2020-01-01'
    ])
);
````

JSON

````json
{
    "filters": [
        {
            "type": "range",
            "range": {
                "gt": "2000-01-01",
                "lt": "2020-01-01"
            }
        }
    ]
}
````

# Implementation

## Example Controller with Resource Trait and AutoApiResource

The trait "SearchCriteriaAwareTrait" on the model will add the handleSearchCriteriaRequest method, and the
AutoApiResource will help to unify the api with aliases for columns in the response.

````php
<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Valkyr\CriteriaBuilder\SearchCriteria;
use Valkyr\CriteriaBuilder\Services\Laravel\Resource\AutoApiResource;

class SearchController extends Controller
{
    /**
     * @param Request $request
     * @param SearchCriteria $criteria
     * @return JsonResponse
     * @throws Exception
     */
    public function __invoke(Request $request, SearchCriteria $criteria): JsonResponse
    {
        $result = Product::handleSearchCriteriaRequest($request);
        
        // returns collection with pagination from Product::handleSearchCriteriaRequest
        return AutoApiResource::collection($result);
    }
}
````

## Example Laravel Resource Controller

This example shows how you can modify the paginator

````php
<?php

class SearchController extends Controller
{
    /**
     * @param Request $request
     * @param SearchCriteria $criteria
     * @return JsonResponse
     * @throws \Exception
     */
    public function __invoke(Request $request, SearchCriteria $criteria): JsonResponse
    {
        $criteria = (new ConvertJsonToCriteria($request->getContent()))->getCriteria();
        $convert = new ConvertCriteriaToEloquent();
        $paginator = $convert->getBuilder(
            new Product(),
            $criteria
        )->paginate(
            $criteria->getPerPage(), 
            '*', 
            'page', 
            $criteria->getPage()
        );
        
        return AutoApiResource::collection($paginator)->additional([
            "aliases" => Product::API_FILTER_MAPPER
        ]);
    }
}
````

#### Model example

````php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Valkyr\CriteriaBuilder\Services\Laravel\SearchCriteriaAwareTrait;

class Product extends Model
{
    use SearchCriteriaAwareTrait;

    public const API_FILTER_MAPPER = [
        'id' => 'id',
        'name' => 'name',
        'brand_id' => 'brandId',
        'supplier_id' => 'supplierId',
        'parent_id' => 'parentId',
        'created_at' => 'createdAt',
        'updated_at' => 'updatedAt',
    ];

    public $incrementing = false;
    protected $keyType = 'uuid';

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class);
    }
}
````

#### Example JSON request

````json
{
    "perPage": 1,
    "page": 1,
    "filters": [
        {
            "type": "prefix",
            "field": "name",
            "value": "laboriosam"
        }
    ],
    "includes": [
        "id",
        "name",
        "sku",
        "createdAt"
    ],
    "associations": {
        "categories": {
            "includes": [
                "id",
                "parentId",
                "name"
            ],
            "associations": [
                {
                    "association": "parent",
                    "includes": [
                        "id",
                        "urlKey"
                    ],
                    "associations": [
                        {
                            "association": "children",
                            "includes": [
                                "id",
                                "parentId",
                                "name"
                            ],
                            "associations": []
                        }
                    ]
                }
            ]
        },
        "brand": {
            "includes": [
                "id",
                "name"
            ]
        }
    }
}
````

#### JSON response result

````json
{
    "data": [
        {
            "id": "7e1a86b9-3bbd-4cd2-8967-c1c3b35e1c71",
            "name": "laboriosam eos pariatur",
            "sku": "laboriosam-eos-pariatur-1",
            "createdAt": "2021-06-19 11:18:26",
            "associations": {
                "categories": [
                    {
                        "id": "c7362371-0e01-4e12-8cf9-f8fa1ec05d29",
                        "parentId": "e21aa3f2-abc9-4c50-972c-83a829d6e2a7",
                        "name": "Araceli Gibson",
                        "associations": {
                            "pivot": {
                                "product_id": "7e1a86b9-3bbd-4cd2-8967-c1c3b35e1c71",
                                "category_id": "c7362371-0e01-4e12-8cf9-f8fa1ec05d29"
                            },
                            "parent": {
                                "id": "e21aa3f2-abc9-4c50-972c-83a829d6e2a7",
                                "urlKey": "verlie-schaefer",
                                "associations": {
                                    "children": [
                                        {
                                            "id": "6df2f8db-3035-453a-8d96-ce547b6625db",
                                            "parentId": "e21aa3f2-abc9-4c50-972c-83a829d6e2a7",
                                            "name": "Dr. Aditya Schultz"
                                        },
                                        {
                                            "id": "c7362371-0e01-4e12-8cf9-f8fa1ec05d29",
                                            "parentId": "e21aa3f2-abc9-4c50-972c-83a829d6e2a7",
                                            "name": "Araceli Gibson"
                                        },
                                        {
                                            "id": "101fa3e8-3b3b-4591-bc52-dbef2cda2ce2",
                                            "parentId": "e21aa3f2-abc9-4c50-972c-83a829d6e2a7",
                                            "name": "Elisa Greenholt"
                                        }
                                    ]
                                }
                            }
                        }
                    },
                    {
                        "id": "c7362371-0e01-4e12-8cf9-f8fa1ec05d29",
                        "parentId": "e21aa3f2-abc9-4c50-972c-83a829d6e2a7",
                        "name": "Araceli Gibson",
                        "associations": {
                            "pivot": {
                                "product_id": "7e1a86b9-3bbd-4cd2-8967-c1c3b35e1c71",
                                "category_id": "c7362371-0e01-4e12-8cf9-f8fa1ec05d29"
                            },
                            "parent": {
                                "id": "e21aa3f2-abc9-4c50-972c-83a829d6e2a7",
                                "urlKey": "verlie-schaefer",
                                "associations": {
                                    "children": [
                                        {
                                            "id": "6df2f8db-3035-453a-8d96-ce547b6625db",
                                            "parentId": "e21aa3f2-abc9-4c50-972c-83a829d6e2a7",
                                            "name": "Dr. Aditya Schultz"
                                        },
                                        {
                                            "id": "c7362371-0e01-4e12-8cf9-f8fa1ec05d29",
                                            "parentId": "e21aa3f2-abc9-4c50-972c-83a829d6e2a7",
                                            "name": "Araceli Gibson"
                                        },
                                        {
                                            "id": "101fa3e8-3b3b-4591-bc52-dbef2cda2ce2",
                                            "parentId": "e21aa3f2-abc9-4c50-972c-83a829d6e2a7",
                                            "name": "Elisa Greenholt"
                                        }
                                    ]
                                }
                            }
                        }
                    },
                    {
                        "id": "add131b6-871f-416b-a919-26af7d693b44",
                        "parentId": "490ad1bf-382d-4f08-94c3-11339120a998",
                        "name": "Baron Oberbrunner",
                        "associations": {
                            "pivot": {
                                "product_id": "7e1a86b9-3bbd-4cd2-8967-c1c3b35e1c71",
                                "category_id": "add131b6-871f-416b-a919-26af7d693b44"
                            },
                            "parent": {
                                "id": "490ad1bf-382d-4f08-94c3-11339120a998",
                                "urlKey": "reva-ledner",
                                "associations": {
                                    "children": [
                                        {
                                            "id": "8d08f162-eeab-4fd8-87db-66d97ab4ed70",
                                            "parentId": "490ad1bf-382d-4f08-94c3-11339120a998",
                                            "name": "Miss Mia Weimann II"
                                        },
                                        {
                                            "id": "add131b6-871f-416b-a919-26af7d693b44",
                                            "parentId": "490ad1bf-382d-4f08-94c3-11339120a998",
                                            "name": "Baron Oberbrunner"
                                        },
                                        {
                                            "id": "7bc9e1ad-a80c-4549-a284-243aa7967bdb",
                                            "parentId": "490ad1bf-382d-4f08-94c3-11339120a998",
                                            "name": "Margret Batz DDS"
                                        }
                                    ]
                                }
                            }
                        }
                    },
                    {
                        "id": "9b151afa-9eac-4379-9748-167edcee0d6b",
                        "parentId": "02d9cdb6-8dd6-4f90-a2cf-9cebd471ba16",
                        "name": "Mr. Dusty Herman",
                        "associations": {
                            "pivot": {
                                "product_id": "7e1a86b9-3bbd-4cd2-8967-c1c3b35e1c71",
                                "category_id": "9b151afa-9eac-4379-9748-167edcee0d6b"
                            },
                            "parent": {
                                "id": "02d9cdb6-8dd6-4f90-a2cf-9cebd471ba16",
                                "urlKey": "deron-donnelly",
                                "associations": {
                                    "children": [
                                        {
                                            "id": "75fd517a-985e-4195-806f-cc4ead269591",
                                            "parentId": "02d9cdb6-8dd6-4f90-a2cf-9cebd471ba16",
                                            "name": "Orpha Schmeler"
                                        },
                                        {
                                            "id": "9b151afa-9eac-4379-9748-167edcee0d6b",
                                            "parentId": "02d9cdb6-8dd6-4f90-a2cf-9cebd471ba16",
                                            "name": "Mr. Dusty Herman"
                                        },
                                        {
                                            "id": "cb3eb617-4493-4def-a5a2-95ea79c06ed6",
                                            "parentId": "02d9cdb6-8dd6-4f90-a2cf-9cebd471ba16",
                                            "name": "Angeline Bergnaum"
                                        }
                                    ]
                                }
                            }
                        }
                    },
                    {
                        "id": "24ab92b1-97bc-4a74-ac3d-10fed8c6d1a5",
                        "parentId": "4f9e7240-d026-45df-818d-bb7ddfd1d8ce",
                        "name": "Bettye Stanton",
                        "associations": {
                            "pivot": {
                                "product_id": "7e1a86b9-3bbd-4cd2-8967-c1c3b35e1c71",
                                "category_id": "24ab92b1-97bc-4a74-ac3d-10fed8c6d1a5"
                            },
                            "parent": {
                                "id": "4f9e7240-d026-45df-818d-bb7ddfd1d8ce",
                                "urlKey": "frederick-senger-v",
                                "associations": {
                                    "children": [
                                        {
                                            "id": "8b8ba9e7-e858-4afa-8b12-81b8e09dbefd",
                                            "parentId": "4f9e7240-d026-45df-818d-bb7ddfd1d8ce",
                                            "name": "Jeffrey Mante"
                                        },
                                        {
                                            "id": "24ab92b1-97bc-4a74-ac3d-10fed8c6d1a5",
                                            "parentId": "4f9e7240-d026-45df-818d-bb7ddfd1d8ce",
                                            "name": "Bettye Stanton"
                                        },
                                        {
                                            "id": "1c954aee-3905-4932-a676-d544220b3cbc",
                                            "parentId": "4f9e7240-d026-45df-818d-bb7ddfd1d8ce",
                                            "name": "Jessie Dare"
                                        }
                                    ]
                                }
                            }
                        }
                    }
                ]
            }
        }
    ],
    "links": {
        "first": "http://localhost/api/test?page=1",
        "last": "http://localhost/api/test?page=34",
        "prev": null,
        "next": "http://localhost/api/test?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 34,
        "links": [
            "...snipp..."
        ],
        "path": "http://localhost/api/test",
        "per_page": 1,
        "to": 1,
        "total": 34
    }
}
````

# Notes

Include parent ids if using nested associations

````json
{
    "perPage": 4,
    "page": 3,
    "filters": [
        {
            "type": "prefix",
            "field": "name",
            "value": "laboriosam"
        }
    ],
    "includes": [],
    "associations": {
        "categories": {
            "associations": [
                {
                    "association": "parent",
                    "includes": [
                        "id",
                        "urlKey"
                    ],
                    "associations": [
                        {
                            "association": "children",
                            "includes": [
                                "id",
                                "parentId",
                                // ^^^^ Need to display these children
                                "name"
                            ],
                            "associations": []
                        }
                    ]
                }
            ]
        },
        "brand": {
            "includes": [
                "id",
                "name"
            ]
        }
    }
}
````
