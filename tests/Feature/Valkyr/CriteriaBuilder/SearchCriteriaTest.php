<?php

namespace Valkyr\CriteriaBuilder;

use PHPUnit\Framework\TestCase;
use Valkyr\CriteriaBuilder\Criteria\Association;
use Valkyr\CriteriaBuilder\Filter\ContainsFilter;
use Valkyr\CriteriaBuilder\Filter\EqualsAnyFilter;
use Valkyr\CriteriaBuilder\Filter\EqualsFilter;
use Valkyr\CriteriaBuilder\Filter\MultiFilter;
use Valkyr\CriteriaBuilder\Filter\NotFilter;
use Valkyr\CriteriaBuilder\Filter\PrefixFilter;
use Valkyr\CriteriaBuilder\Filter\SuffixFilter;

class SearchCriteriaTest extends TestCase
{
    /**
     * @covers \Valkyr\CriteriaBuilder\SearchCriteria::jsonSerialize()
     * @throws \Exception
     */
    public function testJsonSerialize()
    {
        $criteria = new SearchCriteria();
        $criteria->addFilter(new EqualsAnyFilter('name', ['test']));
        $criteria->addFilter(new ContainsFilter('name', 'test'));
        $criteria->addFilter(new SuffixFilter('name', 'test'));
        $criteria->addFilter(new PrefixFilter('name', 'test'));

        $criteria->addFilter(new NotFilter(NotFilter::CONNECTION_AND, [
            new EqualsAnyFilter('name', ['test', 'test2']),
            new PrefixFilter('name', 'test'),
            new SuffixFilter('name', 'test'),
            new EqualsFilter('name', 'test'),
            new MultiFilter(MultiFilter::CONNECTION_AND, [
                new EqualsAnyFilter('name', ['test', 'test2']),
                new PrefixFilter('name', 'test'),
                new SuffixFilter('name', 'test'),
                new EqualsFilter('name', 'test'),
            ])
        ]));


        $criteria->addFilter(
            new MultiFilter(MultiFilter::CONNECTION_AND, [
                new EqualsFilter('first_name', 'test'),
                new MultiFilter(MultiFilter::CONNECTION_OR, [
                    new EqualsFilter('something', 'FOO Yolosson'),
                    new EqualsFilter('last_thing', 'YOO'),
                ])
            ]));

        $criteria->addAssociation(new Association('categories', ['name', 'id'], []));

        $test = '{"associations":[{"association":"categories","includes":["name","id"],"filters":[],"associations":[]}],"queries":[],"filters":[{"type":"equalsAny","field":"name","value":["test"]},{"type":"contains","field":"name","value":"test"},{"type":"suffix","field":"name","value":"test"},{"type":"prefix","field":"name","value":"test"},{"type":"not","operator":"and","queries":[{"type":"equalsAny","field":"name","value":["test","test2"]},{"type":"prefix","field":"name","value":"test"},{"type":"suffix","field":"name","value":"test"},{"type":"equals","field":"name","value":"test"},{"type":"multi","operator":"and","queries":[{"type":"equalsAny","field":"name","value":["test","test2"]},{"type":"prefix","field":"name","value":"test"},{"type":"suffix","field":"name","value":"test"},{"type":"equals","field":"name","value":"test"}]}]},{"type":"multi","operator":"and","queries":[{"type":"equals","field":"first_name","value":"test"},{"type":"multi","operator":"or","queries":[{"type":"equals","field":"something","value":"FOO Yolosson"},{"type":"equals","field":"last_thing","value":"YOO"}]}]}],"includes":[],"sort":[],"perPage":10,"page":null}';
        self::assertEquals(json_encode($criteria), $test);
    }
}
