<?php

namespace Valkyr\CriteriaBuilder\Services;

use Exception;
use PHPUnit\Framework\TestCase;
use Valkyr\CriteriaBuilder\Criteria\Association;
use Valkyr\CriteriaBuilder\Filter\ContainsFilter;
use Valkyr\CriteriaBuilder\Filter\EqualsAnyFilter;
use Valkyr\CriteriaBuilder\Filter\EqualsFilter;
use Valkyr\CriteriaBuilder\Filter\MultiFilter;
use Valkyr\CriteriaBuilder\Filter\NotFilter;
use Valkyr\CriteriaBuilder\Filter\PrefixFilter;
use Valkyr\CriteriaBuilder\Filter\SuffixFilter;
use Valkyr\CriteriaBuilder\SearchCriteria;

class ConvertJsonToCriteriaTest extends TestCase
{

    /**
     * @covers \Valkyr\CriteriaBuilder\Services\ConvertJsonToCriteria::getCriteria
     * @throws Exception
     */
    public function testGetCriteria()
    {
        $criteria = new SearchCriteria();
        $criteria->addFilter(new EqualsAnyFilter('name', ['test']));
        $criteria->addFilter(new ContainsFilter('name', 'test'));
        $criteria->addFilter(new SuffixFilter('name', 'test'));
        $criteria->addFilter(new PrefixFilter('name', 'test'));

        $criteria->addFilter(new NotFilter(NotFilter::CONNECTION_AND, [
            new EqualsAnyFilter('name', ['test', 'test2']),
            new PrefixFilter('name', 'test'),
            new SuffixFilter('name', 'test'),
            new EqualsFilter('name', 'test'),
            new MultiFilter(MultiFilter::CONNECTION_AND, [
                new EqualsAnyFilter('name', ['test', 'test2']),
                new PrefixFilter('name', 'test'),
                new SuffixFilter('name', 'test'),
                new EqualsFilter('name', 'test'),
            ])
        ]));


        $criteria->addFilter(
            new MultiFilter(MultiFilter::CONNECTION_AND, [
                new EqualsFilter('first_name', 'test'),
                new MultiFilter(MultiFilter::CONNECTION_OR, [
                    new EqualsFilter('something', 'FOO Yolosson'),
                    new EqualsFilter('last_thing', 'YOO'),
                ])
            ]));

        $criteria->addAssociation(new Association('categories', ['name', 'id'], []));

        $json = json_encode($criteria);
        $convert = new ConvertJsonToCriteria($json);

        self::assertEquals(json_encode($criteria), json_encode($convert->getCriteria()), 'Convert criteria to json and back to criteria');
    }
}
