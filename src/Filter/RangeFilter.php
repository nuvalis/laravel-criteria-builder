<?php

declare(strict_types=1);

namespace Valkyr\CriteriaBuilder\Filter;

use Valkyr\CriteriaBuilder\Contracts\FilterInterface;

class RangeFilter implements \JsonSerializable, FilterInterface
{
    public const TYPE = 'range';

    public const LTE = 'lte';
    public const GTE = 'gte';
    public const LT = 'lt';
    public const GT = 'gt';

    public const ALLOWED_KEYS = [
        self::LTE,
        self::GTE,
        self::LT,
        self::GT
    ];

    private string $type;
    private string $field;
    private array $range;

    /**
     * @throws \Exception
     */
    public function __construct($field, array $range)
    {
        foreach ($range as $key => $value) {
            if (!in_array($key, self::ALLOWED_KEYS)) {
                throw new \Exception(
                    sprintf(
                        'Unknown key found expected one of [ %s ]',
                        implode(', ', self::ALLOWED_KEYS)
                    )
                );
            }
        }

        $this->type = self::TYPE;
        $this->field = $field;
        $this->range = $range;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getRange(): array
    {
        return $this->range;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }
}
