<?php

namespace Valkyr\CriteriaBuilder\Filter;

use Exception;
use Valkyr\CriteriaBuilder\Contracts\FilterInterface;

class NotFilter implements \JsonSerializable, FilterInterface
{
    public const TYPE = 'not';
    public const CONNECTION_OR = 'or';
    public const CONNECTION_AND = 'and';
    public const OPERATORS = [
        self::CONNECTION_AND,
        self::CONNECTION_OR,
    ];

    private string $type;
    private string $operator;
    private array $queries;

    /**
     * @throws Exception
     */
    public function __construct($operator, array $queries)
    {
        if (!in_array($operator, self::OPERATORS)) {
            throw new Exception(
                sprintf(
                    'Unknown operator expected one of [ %s ]',
                    implode(', ', self::OPERATORS)
                )
            );
        }

        $this->type = self::TYPE;
        $this->operator = $operator;

        $this->queries = array_map(function (FilterInterface $query) {
            return $query;
        }, $queries);
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @return FilterInterface[]
     */
    public function getQueries(): array
    {
        return $this->queries;
    }
}
