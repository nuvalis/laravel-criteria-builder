<?php

namespace Valkyr\CriteriaBuilder\Filter;

use Valkyr\CriteriaBuilder\Contracts\FilterInterface;

class EqualsFilter implements \JsonSerializable, FilterInterface
{
    public const TYPE = 'equals';
    private string $type;
    private string $field;
    private string $value;

    /**
     * Equals constructor.
     * @param string $field
     * @param string $value
     */
    public function __construct(string $field, string $value)
    {
        $this->field = $field;
        $this->value = $value;
        $this->type = self::TYPE;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
