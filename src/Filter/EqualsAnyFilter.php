<?php

namespace Valkyr\CriteriaBuilder\Filter;

use Valkyr\CriteriaBuilder\Contracts\FilterInterface;

class EqualsAnyFilter implements \JsonSerializable, FilterInterface
{
    public const TYPE = 'equalsAny';
    private string $type;
    private string $field;
    private array $value;

    /**
     * Equals constructor.
     * @param string $field
     * @param array $value
     */
    public function __construct(string $field, array $value)
    {
        $this->field = $field;
        $this->value = $value;
        $this->type = self::TYPE;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->value;
    }
}
