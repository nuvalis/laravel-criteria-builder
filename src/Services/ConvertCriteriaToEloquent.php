<?php

namespace Valkyr\CriteriaBuilder\Services;

use Exception;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Valkyr\CriteriaBuilder\Criteria\Association;
use Valkyr\CriteriaBuilder\Criteria\Sort;
use Valkyr\CriteriaBuilder\Filter\ContainsFilter;
use Valkyr\CriteriaBuilder\Filter\EqualsAnyFilter;
use Valkyr\CriteriaBuilder\Filter\EqualsFilter;
use Valkyr\CriteriaBuilder\Filter\MultiFilter;
use Valkyr\CriteriaBuilder\Filter\NotFilter;
use Valkyr\CriteriaBuilder\Filter\PrefixFilter;
use Valkyr\CriteriaBuilder\Filter\RangeFilter;
use Valkyr\CriteriaBuilder\Filter\SuffixFilter;
use Valkyr\CriteriaBuilder\SearchCriteria;
use Str;

class ConvertCriteriaToEloquent
{
    /**
     * @param Model $model
     * @param SearchCriteria $criteria
     * @return EloquentBuilder|QueryBuilder
     * @throws Exception
     */
    public function getBuilder(Model $model, SearchCriteria $criteria)
    {
        if (!$model::API_FILTER_MAPPER ?? null) {
            throw new Exception(
                sprintf(
                    'Constant "API_FILTER_MAPPER" is not available on this model: %s,
                    Expected:> $model::API_FILTER_MAPPER = [ "original_column" => "aliasColumn" ]',
                    $model::class
                )
            );
        }

        $includes = $criteria->getIncludes();
        $filters = $criteria->getFilters();
        $associations = $criteria->getAssociations();
        $sort = $criteria->getSort();

        $builder = $model->newQuery();
        $builder = $this->handleIncludes($model, $builder, $includes);
        $builder = $this->handleFilters($model, $builder, $filters);
        $builder = $this->handleAssociation($model, $builder, $associations);
        $builder = $this->handleSort($model, $builder, $sort);

        return $builder;
    }

    /**
     * @param Model $model
     * @param EloquentBuilder|QueryBuilder $builder
     * @param array $includes
     * @return EloquentBuilder|QueryBuilder
     * @throws Exception
     */
    private function handleIncludes(Model $model, $builder, array $includes)
    {
        if (count($includes)) {
            return $builder->select(array_map(function (string $column) use ($model) {
                return $this->convertFieldAliasToColumn($model, $column);
            }, $includes));
        }
        return $builder;
    }

    private function convertFieldAliasToColumn(Model $model, string $string): string
    {
        $parts = explode('.', $string);

        $newParts = [];
        foreach ($parts as $part) {
            $alias = array_flip($model::API_FILTER_MAPPER)[$string] ?? null;

            if ($alias) {
                $newParts[] = $alias;
                continue;
            }

            $newParts[] = Str::snake($part);
        }


        return implode('->', $newParts);
    }

    /**
     * @param Model $model
     * @param EloquentBuilder|QueryBuilder $builder
     * @param array $filters
     * @return EloquentBuilder|QueryBuilder
     * @throws Exception
     */
    public function handleFilters(Model $model, $builder, array $filters)
    {
        $connection = config('database.default');
        $driver = config("database.connections.{$connection}.driver");

        $like = match ($driver) {
            'pgsql' => 'ILIKE',
            default => 'LIKE',
        };

        foreach ($filters as $filter) {
            $field = $this->convertFieldAliasToColumn($model, $filter->getField());
            $isJson = false;
            if (str_contains($field, '->')) {
                $isJson = true;
            }

            switch ($filter->getType()) {
                case ContainsFilter::TYPE:
                    /** @var ContainsFilter $filter */
                    if ($isJson) {
                        $builder->jsonSearch($field, $like, sprintf('%%%s%%', $filter->getValue()));
                        //$builder->whereJsonContains($field, [$filter->getValue()]);
                    } else {
                        $builder->where($field, $like, sprintf('%%%s%%', $filter->getValue()));
                    }
                    break;
                case EqualsAnyFilter::TYPE:
                    /** @var EqualsAnyFilter $filter */
                    $builder->whereIn($field, $filter->getValue());
                    break;
                case EqualsFilter::TYPE:
                    /** @var EqualsFilter $filter */
                    $builder->where($field, $filter->getValue());
                    break;
                case MultiFilter::TYPE:
                    /** @var MultiFilter $filter */
                    if ($filter->getOperator() === MultiFilter::CONNECTION_OR) {
                        $builder->orWhere(function ($query) use ($filter, $model) {
                            return $this->handleFilters($model, $query, $filter->getQueries());
                        });
                    } else {
                        $builder->where(function ($query) use ($filter, $model) {
                            return $this->handleFilters($model, $query, $filter->getQueries());
                        });
                    }
                    break;
                case NotFilter::TYPE:
                    /**
                     * @link https://github.com/laravel/ideas/issues/708
                     * @var NotFilter $filter
                     */
                    if ($filter->getOperator() === NotFilter::CONNECTION_OR) {
                        $builder->where(function ($query) use ($filter, $model) {
                            return $this->handleFilters($model, $query, $filter->getQueries());
                        }, null, null, 'or not');
                    } else {
                        $builder->where(function ($query) use ($filter, $model) {
                            return $this->handleFilters(
                                $model,
                                $query->from($model->getTable()),
                                $filter->getQueries()
                            );
                        }, null, null, 'and not');
                    }
                    break;
                case PrefixFilter::TYPE:
                    /** @var PrefixFilter $filter */
                    $builder->where($field, $like, sprintf('%s%%', $filter->getValue()));
                    break;
                case SuffixFilter::TYPE:
                    /** @var SuffixFilter $filter */
                    $builder->where($field, $like, sprintf('%%%s', $filter->getValue()));
                    break;
                case RangeFilter::TYPE:
                    /** @var RangeFilter $filter */
                    $builder->where(function ($query) use ($filter, $model) {
                        $field = $this->convertFieldAliasToColumn($model, $filter->getField());
                        foreach ($filter->getRange() as $operator => $value) {
                            $query->where($field, $this->convertRangeOperator($operator), $value);
                        }
                    });
                    break;
                default:
                    throw new Exception(sprintf('Filter "%s" is not available', $filter->getType()));
            }
        }
        return $builder;
    }

    /**
     * @param $token
     * @return string
     */
    private function convertRangeOperator($token): string
    {
        $operatorMap = [
            RangeFilter::GT => '>',
            RangeFilter::LT => '<',
            RangeFilter::LTE => '<=',
            RangeFilter::GTE => '>=',
        ];

        return $operatorMap[$token];
    }

    /**
     * @param Model $model
     * @param EloquentBuilder|QueryBuilder $builder
     * @param array $associations
     * @return EloquentBuilder|QueryBuilder
     */
    public function handleAssociation($parentModel, $builder, array $associations)
    {
        $with = [];

        /** @var Association $association */
        foreach ($associations as $association) {
            /**
             * @return EloquentBuilder|QueryBuilder
             * @var EloquentBuilder|QueryBuilder $query
             */
            $with[$association->getAssociation()] = function ($query) use ($association, $parentModel) {
                /** @var Category $parentModel */
                $scopedModel = $parentModel->{$association->getAssociation()}()->newModelInstance();

                if (count($association->getIncludes())) {
                    $query = $query->select(array_map(function ($columnString) use ($scopedModel) {
                        return $this->convertFieldAliasToColumn($scopedModel, $columnString);
                    }, $association->getIncludes()));
                } else {
                    $query = $query->select('*');
                }

                if (count($association->getFilters())) {
                    $query = $this->handleFilters($scopedModel, $query, $association->getFilters());
                }

                if (count($association->getAssociations())) {
                    $query = $this->handleAssociation($scopedModel, $query, $association->getAssociations());
                }

                return $query;
            };
        }

        return $builder->with($with);
    }

    private function handleSort(
        Model $model,
        EloquentBuilder|QueryBuilder $builder,
        array $sort
    ): EloquentBuilder|QueryBuilder {
        /**
         * @var $s Sort
         */
        foreach ($sort as $s) {
            $builder->orderBy($this->convertFieldAliasToColumn($model, $s->getField()), $s->getDirection());
        }
        return $builder;
    }
}
