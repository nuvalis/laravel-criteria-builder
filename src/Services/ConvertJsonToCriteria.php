<?php

namespace Valkyr\CriteriaBuilder\Services;

use Exception;
use Valkyr\CriteriaBuilder\Contracts\FilterInterface;
use Valkyr\CriteriaBuilder\Criteria\Association;
use Valkyr\CriteriaBuilder\Criteria\Sort;
use Valkyr\CriteriaBuilder\Filter\ContainsFilter;
use Valkyr\CriteriaBuilder\Filter\EqualsAnyFilter;
use Valkyr\CriteriaBuilder\Filter\EqualsFilter;
use Valkyr\CriteriaBuilder\Filter\MultiFilter;
use Valkyr\CriteriaBuilder\Filter\NotFilter;
use Valkyr\CriteriaBuilder\Filter\PrefixFilter;
use Valkyr\CriteriaBuilder\Filter\RangeFilter;
use Valkyr\CriteriaBuilder\Filter\SuffixFilter;
use Valkyr\CriteriaBuilder\SearchCriteria;

class ConvertJsonToCriteria
{
    private SearchCriteria $criteria;

    /**
     * ConvertJsonToCriteria constructor.
     * @param string $json
     * @throws Exception
     */
    public function __construct(string $json)
    {
        $data = json_decode($json, true);
        $this->criteria = new SearchCriteria();

        if ($data['page'] ?? null) {
            $this->criteria->setPage($data['page']);
        }

        if ($data['perPage'] ?? null) {
            $this->criteria->setPerPage($data['perPage']);
        }

        if ($data['cursor'] ?? null) {
            $this->criteria->setCursor($data['cursor'] ?? null);
        }

        foreach ($data['sort'] ?? [] as $sort) {
            $obj = new Sort($sort['field'], $sort['direction']);
            $this->criteria->addSort($obj);
        }

        foreach ($data['filters'] ?? [] as $filter) {
            $this->criteria->addFilter($this->convertToFilterObject($filter));
        }

        foreach ($data['includes'] ?? [] as $include) {
            $this->criteria->addInclude($include);
        }

        foreach ($data['associations'] ?? [] as $key => $association) {
            $this->criteria->addAssociation(new Association(
                is_numeric($key) ? $association['association'] : $key,
                $association['includes'] ?? [],
                collect($association['filters'] ?? [])->map(fn($filter) => $this->convertToFilterObject($filter))->toArray(),
                collect($association['associations'] ?? [])->map(fn($association) => $this->convertToAssociationObject($association))->toArray(),
            ));
        }
    }

    /**
     * @param $filter
     * @return FilterInterface
     * @throws Exception
     */
    private function convertToFilterObject($filter): FilterInterface
    {
        return match ($filter['type']) {
            ContainsFilter::TYPE => new ContainsFilter($filter['field'], $filter['value']),
            EqualsAnyFilter::TYPE => new EqualsAnyFilter($filter['field'], $filter['value']),
            EqualsFilter::TYPE => new EqualsFilter($filter['field'], $filter['value']),
            MultiFilter::TYPE => new MultiFilter(
                $filter['operator'], array_map(function ($filter) {
                return $this->convertToFilterObject($filter);
            }, $filter['queries'])
            ),
            NotFilter::TYPE => new NotFilter(
                $filter['operator'], array_map(function ($filter) {
                return $this->convertToFilterObject($filter);
            }, $filter['queries'])
            ),
            PrefixFilter::TYPE => new PrefixFilter($filter['field'], $filter['value']),
            SuffixFilter::TYPE => new SuffixFilter($filter['field'], $filter['value']),
            RangeFilter::TYPE => new RangeFilter($filter['field'], $filter['range']),
            default => throw new Exception("Unknown filter type '{$filter['type']}'"),
        };
    }

    /**
     * @param $association
     * @return Association
     */
    private function convertToAssociationObject($association): Association
    {
        return new Association(
            $association['association'],
            $association['includes'] ?? [],
            $association['filters'] ?? [],
            array_map(function ($item) {
                return $this->convertToAssociationObject($item);
            }, $association['associations'] ?? [])
        );
    }

    /**
     * @return SearchCriteria
     */
    public function getCriteria(): SearchCriteria
    {
        return $this->criteria;
    }
}
