<?php

namespace Valkyr\CriteriaBuilder\Services\Laravel;

use App\Services\Entity\EntityMapDefinition;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Valkyr\CriteriaBuilder\Services\ConvertCriteriaToEloquent;
use Valkyr\CriteriaBuilder\Services\ConvertJsonToCriteria;
use Valkyr\CriteriaBuilder\Services\Laravel\Resource\AutoApiResource;

trait SearchCriteriaAwareTrait
{
    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     * @throws Exception
     */
    public static function handleSearchCriteriaRequestWithResource(Request $request): AnonymousResourceCollection
    {
        $paginator = self::handleSearchCriteriaRequest($request);
        return AutoApiResource::collection($paginator)->additional([
            'aliases' => self::API_FILTER_MAPPER,
        ]);
    }

    /**
     * @param Request $request
     * @return LengthAwarePaginator
     * @throws Exception
     */
    public static function handleSearchCriteriaRequest(Request $request, Model|null $model = null)
    {
        $criteria = (new ConvertJsonToCriteria($request->getContent()))->getCriteria();
        $convert = new ConvertCriteriaToEloquent();

        /** @var Model $this */
        return $convert->getBuilder(
            $model ?? new static(),
            $criteria
        )->paginate(
            $criteria->getPerPage(),
            '*',
            'page',
            $criteria->getPage()
        );
    }

    /**
     * @return array
     */
    public function getApiFilterableAliases(): array
    {
        return static::API_FILTER_MAPPER;
    }

    public function getEntityResolver()
    {
        $resolver = \App::make(EntityMapDefinition::class);
        return $resolver->resolveByModel(static::class);
    }
}
