<?php

namespace Valkyr\CriteriaBuilder\Services\Laravel\Resource;

use App;
use Crell\Serde\SerdeCommon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Valkyr\CriteriaBuilder\Services\Laravel\SearchCriteriaAwareTrait;

class AutoApiResource extends JsonResource
{
    private SerdeCommon $serde;

    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->serde = App::make(SerdeCommon::class);
    }

    /**
     * This function will auto convert columns
     * and relationships to new api aliases, the models are required to
     * use the SearchCriteriaAwareTrait
     *
     * @param Request $request
     * @return array
     * @see SearchCriteriaAwareTrait
     */
    public function toArray(Request $request): array
    {
        /**
         * @var $this Model|SearchCriteriaAwareTrait
         */
        $attributes = $this->resource->toArray();

        if (!method_exists($this->resource, 'getEntityResolver')) {
            return [
                'NOT_IMPLEMENTED' => []
            ];
        }

        $resolver = $this->resource->getEntityResolver();

        if (!$resolver) {
            return [];
        }

        $resource = $this->serde->deserialize(
            $attributes,
            'array',
            $resolver->getTransportClass(),
            ['database']
        );

        $payload = $this->serde->serialize($resource, 'array', ['api']);
        $payload['associations'] = [];
        $payload['entityAlias'] = $resolver->getEntityAlias();

        $relations = $this->getRelations();
        if ($relations) {
            foreach ($relations as $key => $relation) {
                if ($relation instanceof Pivot) {
                    continue;
                }

                if (is_iterable($relation)) {
                    $payload['associations'][$key] = AutoApiResource::collection($relation)->toArray($request);
                }

                if ($relation instanceof Model) {
                    $payload['associations'][$key] = AutoApiResource::make($relation)->toArray($request);
                }
            }
        }

        return $payload;
    }
}
