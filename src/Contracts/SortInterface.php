<?php

namespace Valkyr\CriteriaBuilder\Contracts;

interface SortInterface
{
    public function setDirection(string $direction);
    public function getDirection(): string;
    public function setField(string $field);
    public function getField(): string;
}
