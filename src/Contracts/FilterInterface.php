<?php

namespace Valkyr\CriteriaBuilder\Contracts;

interface FilterInterface
{
    public function getType(): string;
}
