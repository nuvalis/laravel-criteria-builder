<?php

namespace Valkyr\CriteriaBuilder;

use Valkyr\CriteriaBuilder\Contracts\AssociationInterface;
use Valkyr\CriteriaBuilder\Contracts\FilterInterface;
use Valkyr\CriteriaBuilder\Contracts\QueryInterface;
use Valkyr\CriteriaBuilder\Contracts\SortInterface;

class SearchCriteria implements \JsonSerializable
{
    private array $associations = [];
    private array $queries = [];
    private array $filters = [];
    private array $includes = [];
    private array $sort = [];
    private int $perPage = 10;
    private ?int $page = null;
    private string $cursor;

    /**
     * @param QueryInterface $query
     */
    public function addQuery(QueryInterface $query)
    {
        $this->queries[] = $query;
    }

    /**
     * @param FilterInterface $filter
     */
    public function addFilter(FilterInterface $filter)
    {
        $this->filters[] = $filter;
    }

    /**
     * @param AssociationInterface $association
     */
    public function addAssociation(AssociationInterface $association)
    {
        $this->associations[] = $association;
    }

    /**
     * @param string $include
     */
    public function addInclude(string $include)
    {
        $this->includes[] = $include;
    }

    /**
     * @param SortInterface $sort
     */
    public function addSort(SortInterface $sort)
    {
        $this->sort[] = $sort;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }

    /**
     * @return array
     */
    public function getAssociations(): array
    {
        return $this->associations;
    }

    /**
     * @return array
     */
    public function getQueries(): array
    {
        return $this->queries;
    }

    /**
     * @return FilterInterface[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @return array
     */
    public function getIncludes(): array
    {
        return $this->includes;
    }

    /**
     * @return array
     */
    public function getSort(): array
    {
        return $this->sort;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     */
    public function setPerPage(int $perPage): void
    {
        $this->perPage = $perPage;
    }

    /**
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return string|null
     */
    public function getCursor(): ?string
    {
        return $this->cursor ?? null;
    }

    /**
     * @param string $cursor
     */
    public function setCursor(string $cursor): void
    {
        $this->cursor = $cursor;
    }
}
