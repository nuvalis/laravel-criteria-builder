<?php

namespace Valkyr\CriteriaBuilder\Criteria;

use Valkyr\CriteriaBuilder\Contracts\SortInterface;

class Sort implements SortInterface
{
    private string $field;
    private string $direction;

    public function __construct(string $field, string $direction)
    {
        $this->field = $field;
        $this->direction = $direction;
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function setField(string $field): void
    {
        $this->field = $field;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }

    public function setDirection(string $direction): void
    {
        $direction = in_array($direction, ['asc', 'desc']) ? $direction : 'asc';
        $this->direction = $direction;
    }

}
