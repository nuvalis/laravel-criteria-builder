<?php

namespace Valkyr\CriteriaBuilder\Criteria;

use JsonSerializable;
use Valkyr\CriteriaBuilder\Contracts\AssociationInterface;

class Association implements JsonSerializable, AssociationInterface
{
    private string $association;
    private array $includes;
    private array $filters;
    private array $associations;

    /**
     * Association constructor.
     * @param string $association
     * @param array $includes
     * @param array $filters
     * @param array $associations
     */
    public function __construct(
        string $association,
        array $includes = [],
        array $filters = [],
        array $associations = []
    ) {
        $this->association = $association;
        $this->includes = $includes;
        $this->filters = $filters;
        $this->associations = $associations;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }

    /**
     * @return array
     */
    public function getIncludes(): array
    {
        return $this->includes;
    }

    /**
     * @return string
     */
    public function getAssociation(): string
    {
        return $this->association;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @return array
     */
    public function getAssociations(): array
    {
        return $this->associations;
    }
}
